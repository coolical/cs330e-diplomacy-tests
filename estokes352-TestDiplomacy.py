
# imports
from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_solve


# TestDiplomacy
class TestDiplomacy(TestCase):

    # Simple 1 v 1 + 1 (support), all 3 actions
    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    # simple 1 v 1, no support
    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")

    # 3 armies in one city (1 hold, 2 move), 2 receive 1 support each, 1 with none, all die. 2 supporters live.
    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n"
                     "D Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    # test that support is lost when supporter dies and sorting is applied. Emery made up, check.
    def test_solve_4(self):
        r = StringIO("B NewYork Hold\nD SanFrancisco Support B\nA Barcelona Move Madrid\n"
                     "E Austin Move Washington\nZ Dallas Move Madrid\nC Washington Support Z\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB NewYork\nC [dead]\nD SanFrancisco\nE [dead]\nZ [dead]\n")

    # All die, tests loss of support when supporter attacked
    def test_solve_5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")


''' 

Acceptance Tests (RunDiplomacyN. in/out)
    1. Taken from given tests, simple.
    2. No conflicts, but all three actions used.
    3. Only conflict, no survivors.
    4. Multi-army conflict, many supporters, some supporters attacked, 1 winner.
    5. Multi-army conflict, many supporters, some supporters attacked, no winnners.

'''


if __name__ == "__main__":  # pragma: no cover
    main()

